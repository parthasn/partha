<?php
 	session_start();
 	if(isset($_POST['submit'])) {
	 	$prefix = $_POST['prefix'];
	 	$first_name = $_POST['first_name'];
	 	$middle_name = $_POST['middle_name'];
	 	$last_name = $_POST['last_name'];
	 	$gender = $_POST['gender'];
	 	$date_of_birth = $_POST['date_of_birth'];
	 	$company = $_POST['company'];
	 	$position = $_POST['position'];
	 	$email = $_POST['email'];
	 	$password = $_POST['password'];
	 	$conform_password= $_POST['conform_password'];
	 	$city = $_POST['city'];
	 	$zip = $_POST['zip'];
	 	$state = $_POST['state'];
	 	$permanent_city = $_POST['permanent_city'];
	 	$permanent_zip = $_POST['permanent_zip'];
	 	$permanent_state = $_POST['permanent_state'];
	 	$phone_no = $_POST['phone_no'];
	 	$permanent_address = $_POST['permanent_address'];
	 	$present_address = $_POST['present_address'];
	 	$image = $_FILES['profile_image']['name'];
	 	$error = 0;
	 	#email Validation
		if (filter_var($email, FILTER_VALIDATE_EMAIL));
		else{
			$_SESSION['email_error']="insert a valid email";
			$error++;
		}
		#name validation
		if ((preg_match("/[^a-zA-Z'-]/", $first_name)) || (preg_match("/[^a-zA-Z'-]/", $last_name))){
			$_SESSION['first_name_error'] = 'Include only letters';
			$_SESSION['last_name_error'] = 'Include only letters';
			$error++ ;
		}

		#date of birth validation
		/*
		$interval = $date_of_birth->diff($current_date);
		$current_age = $interval->y;

		if ($current_age>=18);
		else{
			$_SESSION['date_of_birth_error'] = "age must be greater than 18";
		}
		#passsword validation
		if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,12}$/', $password)){
			$_SESSION['error_password'] = "Password must contain 8 letters and it should contain both number and alphabet";
			$error++;
		}*/

		#conform password validation
		if ($password == $conform_password){
			$_SESSION['password'] = $password;
		}
		else{
			$error++;
			$_SESSION['password_match_error'] = "password not matched";
		}

		#zip validation
		if(strlen($zip)==6 && ctype_digit($zip)){
			$_SESSION['zip'] = $zip;
		}
		else{
			$_SESSION['error_zip'] = "invalid zip";
			$error++;
		}

		#phone_no validation
		if(strlen($phone_no)==10 && ctype_digit($phone_no)){
			$_SESSION['phone_no'] = $phone_no;
		}
		else{
			$_SESSION['error_phone_no'] = "invalid phone no";
			$error++;
		}

		#redirection condition
		if ($error == 0) {
			#database operations
		 	$dbc = include("../config.php");
			$unique_email_select = "SELECT * FROM employee WHERE email_id = :email_id LIMIT 1";
			$unique_email_query = $dbc->prepare($unique_email_select);
			$unique_email_query->bindValue('email_id', $email);
			$unique_email_query->execute();
			#Checking of existance of duplicate key

			if ($unique_email_query->fetchColumn() == 1) {
				echo "Duplicate email id in database";
				header("location:registration.php");
			}

			#Image upload
		    if (isset($image)) {
		    	$imageDirectory = '/var/www/html/Assignment/mysqlRegistration/image';
		        $imageName = time().rand().rand().date().'.jpg';
		        $tmp_dir = $_FILES['profile_image']['tmp_name'];
			    move_uploaded_file($tmp_dir, $imageDirectory . '/' . $imageName);
			    $photo_location = $imageName;
			    }
			else {
			    $photo_location = '';
			    }

			$FK_employer_id =  "SELECT * FROM employer WHERE name = :name LIMIT 1";
			$FK_employer_query = $dbc->prepare($FK_employer_id);
			$FK_employer_query->bindValue('name', $company);
			$FK_employer_query->execute();
			$FK_employer_id = "";
			$user = $FK_employer_query->fetch();

			if (!empty($user)) {
				$FK_employer_id = $user['id'];
			}
			else{
				$insert_employer_table = "INSERT INTO employer (name) VALUES (:name)";
			 	$insert_employer = $dbc->prepare($insert_employer_table);
			 	$insert_employer->bindValue(':name', $company, PDO::PARAM_STR);
				$insert_employer->execute();
				$FK_employer_id = $dbc->lastInsertId();
			}

			$FK_employment_id =  "SELECT * FROM employment WHERE name = :name LIMIT 1";
			$FK_employment_query = $dbc->prepare($FK_employment_id);
			$FK_employment_query->bindValue('name', $position);
			$FK_employment_query->execute();
			$FK_employment_id = "";
			$user = $FK_employment_query->fetch();

			if (!empty($user)) {
				$FK_employment_id = $user['id'];
			}
			else{
				$insert_employment_table = "INSERT INTO employment (name) VALUES (:name)";
			 	$insert_employment = $dbc->prepare($insert_employment_table);
			 	$insert_employment->bindValue(':name', $position, PDO::PARAM_STR);
				$insert_employment->execute();
				$FK_employment_id = $dbc->lastInsertId();
			}

			$FK_state_id =  "SELECT * FROM state WHERE name = :name LIMIT 1";
			$FK_state_query = $dbc->prepare($FK_state_id);
			$FK_state_query->bindValue('name', $state);
			$FK_state_query->execute();
			$FK_state_id = "";
			$user = $FK_state_query->fetch();

			if (!empty($user)) {
				$FK_state_id = $user['id'];
			}
			else{
				$insert_state_table = "INSERT INTO state (name) VALUES (:name)";
			 	$insert_state_query = $dbc->prepare($insert_state_table);
			 	$insert_state_query->bindValue(':name', $state, PDO::PARAM_STR);
				$insert_state_query->execute();
				$FK_state_id = $dbc->lastInsertId();
			}

			$FK_city_id =  "SELECT * FROM city WHERE name = :name LIMIT 1";
			$FK_city_query = $dbc->prepare($FK_city_id);	
			$FK_city_query->bindValue(':name', $city);
			$FK_city_query->execute();
			$FK_city_id = "";
			$user = $FK_city_query->fetch();

			if (!empty($user)) {
				$FK_city_id = $user['id'];
			}
			else{
				$insert_city_table = "INSERT INTO city (name , state_id) VALUES (:name, :state_id)";
			 	$insert_city_query = $dbc->prepare($insert_city_table);
			 	$insert_city_query->bindValue(':name', $company, PDO::PARAM_STR);
			 	$insert_city_query->bindValue(':state_id', $FK_state_id, PDO::PARAM_STR);
				$insert_city_query->execute();
				$insert_city_query = $dbc->lastInsertId();
			}

			$password = hash('sha512', $password.'partha');
			$insert_employee_table = "INSERT INTO employee( prefix, first_name, middle_name, last_name, gender, date_of_birth, email_id, password, employer_id, employment_id,photo_location) VALUES (:prefix, :first_name, :middle_name, :last_name, :gender, :date_of_birth, :email_id, :password, :employer_id, :employment_id, :photo_location)";
			$insert_employee_querry = $dbc->prepare($insert_employee_table);
			$insert_employee_querry->bindValue(':prefix' ,$prefix);
			$insert_employee_querry->bindValue(':first_name' ,$first_name);
			$insert_employee_querry->bindValue(':middle_name' ,$middle_name);
			$insert_employee_querry->bindValue(':last_name' ,$last_name);
			$insert_employee_querry->bindValue(':gender' ,$gender);
			$insert_employee_querry->bindValue(':date_of_birth' ,$date_of_birth);	
			$insert_employee_querry->bindValue(':email_id' ,$email);
			$insert_employee_querry->bindValue(':password' ,$password);
			$insert_employee_querry->bindValue(':employer_id' ,$FK_employer_id);
			$insert_employee_querry->bindValue(':employment_id' ,$FK_employment_id);
			$insert_employee_querry->bindValue(':photo_location' ,$photo_location);
			$insert_employee_querry->execute();
			$FK_employee_id = $dbc->lastInsertId();

			if (isset($present_address)) {
				$insert_address_table = "INSERT INTO address(type, location, employee_id, city_id) 
				VALUES (:type, :location, :FK_employee_id , :FK_city_id)";
				$insert_address_querry= $dbc->prepare($insert_address_table);
				$insert_address_querry->bindValue(':type', "present");
				$insert_address_querry->bindValue(':location',$present_address);
				$insert_address_querry->bindValue(':FK_employee_id', $FK_employee_id);
				$insert_address_querry->bindValue(':FK_city_id', $FK_city_id);
				$insert_address_querry->execute();
			}

			if (isset($permanent_address)) {
				$insert_address_table = "INSERT INTO address(type, location, employee_id, city_id) 
				VALUES (:address_type_present, :location, :FK_employee_id , :FK_city_id)";
				$insert_address_querry= $dbc->prepare($insert_address_table);
				$insert_address_querry->bindValue(':address_type_present', "permanent");
				$insert_address_querry->bindValue(':location',$location);
				$insert_address_querry->bindValue(':FK_employee_id', $FK_employee_id);
				$insert_address_querry->bindValue(':FK_city_id', $FK_city_id);
				$insert_address_querry->execute();
			}
			echo 'Congratssss!!!!!!!!!!!!!!!!! you are being redirected now';
        	header("refresh:5;url=login.php");
		}
		else{
			$_SESSION['error'] = true;
			header("location:registration.php");
		}
	}
 ?>																																																																						