<?php
    session_start();

    if(!isset($_SESSION['signin'])){
        header ("Location: login.php");
    }
    $title = "Dashboard";
    include('layout/header.php');

    $dbc = include("../config.php");
    $employee_id_select = "SELECT * FROM employee WHERE id = :id LIMIT 1";
    $querry = $dbc->prepare($employee_id_select);
    $querry->bindValue('id', $_SESSION['user_id']);
    $querry->execute();
    $user = $querry->fetch();
    $img = $user['photo_location'];
?>
<div class="col-md-12 bg-primary">
    <form class="navbar-form navbar-right col-md-3" method="post" action="signout.php">
        <button type="submit" class="btn btn-default" name="sign_out"><span class="glyphicon glyphicon-log-out"></span>  Sign out</button>
    </form>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4"><img src="
                <?php echo isset($img) ? 'image/'.$img : 'http://eadb.org/wp-content/uploads/2015/08/profile-placeholder.jpg'; 
                ?>
                "
                 alt = "profile_image" class = "img-responsive"></div>
        <div class="col-md-8">
            <h1>
                <?php echo $user['first_name'] . " ". $user['middle_name'] . " " . $user['last_name']?>
            </h1>
            <h3>email id: </span>  <?php echo $user['email_id']; ?></h3>
            <h3>DOB: <?php echo $user['date_of_birth']; ?></h3>
        </div>
    </div>  
</div>
<?php
    include ('layout/footer.php');
?>