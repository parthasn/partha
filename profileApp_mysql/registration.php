<?php
    session_start();

    if(isset($_SESSION['signin'])){
        header("location: dashboard.php");
    }

    $title = "Employee Registrationtion";
    include('layout/header.php');
    
 ?>
 
<form class="form-horizontal well" action="register.php" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend class="text-center text-info bg-info">Employee Registration</legend>
        <div class="form-group">
            <label class="col-md-4 control-label" for="prefix">Prefix</label>
            <div class="col-md-4">
                <select id="prefix" name="prefix" class="form-control input-md" required>
                    <option>Mr.</option>
                    <option>Mrs.</option>
                    <option>Miss</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="first_name">First name</label>  
            <div class="col-md-4">
                <input id="first_name" name="first_name" type="text" placeholder="first name" class="form-control input-md" required>
                <span class="text-danger"> <?php echo isset($_SESSION['first_name_error']) ? $_SESSION['first_name_error'] : ''; ?>
                </span> 
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="middle name">Middle Name</label>  
            <div class="col-md-4">
                <input id="middle_name" name="middle_name" type="text" placeholder="middle name" class="form-control input-md">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="ln">Last name</label>  
            <div class="col-md-4">
                <input id="last_name" name="last_name" type="text" placeholder="last name" class="form-control input-md" required="">
                <span class="text-danger"> <?php echo isset($_SESSION['last_name_error']) ? $_SESSION['last_name_error'] : ''; ?></span>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="gender">Gender</label>
            <div class="col-md-4"> 
                <label class="radio-inline" for="gender">
                    <input type="radio" name="gender" id="gender" value="male" checked="checked">
                    male
                    </label> 
                    <label class="radio-inline" for="gender">
                    <input type="radio" name="gender" id="gender" value="female">
                    female
                    </label>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="date_of_birth">Date of Birth</label>  
            <div class="col-md-4">
                <input id="date_of_birth" name="date_of_birth" type="Date" placeholder="date of birth" 
                class="form-control input-md" required>
                <span class="text-danger"> <?php echo isset($_SESSION['date_of_birth_error']) ? 
                $_SESSION['date_of_birth_error'] : ''; ?>
                </span>  
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="cmpny">Company</label>  
            <div class="col-md-4">
                <input id="company" name="company" type="text" placeholder="company" 
                class="form-control input-md" required="">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="position">Position</label>  
            <div class="col-md-4">
                <input id="position" name="position" type="text" placeholder="position" class="form-control input-md" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Email</label>  
            <div class="col-md-4">
                <input id="email" name="email" type="text" placeholder="email" class="form-control input-md" required=""> 
                <span class="text-danger"> <?php echo isset($_SESSION['email_error']) ? $_SESSION['email_error'] : ''; ?></span> 
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="password">Password</label>  
            <div class="col-md-4">
                <input id="password" name="password" type="password" placeholder="password" class="form-control input-md" required="">
                <span class="text-danger"> <?php echo isset($_SESSION['error_password']) ? $_SESSION['error_password'] : ''; ?></span> 
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="conform_password">Conform Password</label>  
            <div class="col-md-4">
                <input id="conform_password" name="conform_password" type="password" placeholder="conform password" class="form-control input-md" required>
                <span class="text-danger"> <?php echo isset($_SESSION['password_match_error']) ? $_SESSION['password_match_error'] : ''; ?></span> 
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="location">Permanent Address</label>  
            <div class="col-md-4">
                <textarea id="permanent_address" name="permanent" type="text" 
                placeholder="address" class="form-control input-md" required>
                </textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="zip">Zip Code</label>  
            <div class="col-md-4">
                <input id="zip" name="permanent_zip" type="text" placeholder="Zip Code" class="form-control input-md"> 
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="city">City</label>  
            <div class="col-md-4">
                <input id="city" name="permanent_city" type="text" placeholder="city" class="form-control input-md" required="">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="state">State</label>  
            <div class="col-md-4">
                <input id="state" name="permanent_state" type="text" placeholder="state" class="form-control input-md" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="present_adddress">Present Address</label>  
            <div class="col-md-4">
                <textarea id="present_address" name="present_address" type="text" 
                placeholder="address" class="form-control input-md" required>
                </textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="zip">Zip Code</label>  
            <div class="col-md-4">
                <input id="zip" name="zip" type="text" placeholder="Zip Code" class="form-control input-md">
                <span class="text-danger"> <?php echo isset($_SESSION['error_zip']) ? $_SESSION['error_zip'] : ''; ?></span> 
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="city">City</label>  
            <div class="col-md-4">
                <input id="present_city" name="city" type="text" placeholder="city" class="form-control input-md" required="">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="state">State</label>  
            <div class="col-md-4">
                <input id="state" name="state" type="text" placeholder="state" class="form-control input-md" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="phone">Text InputPhone</label>  
            <div class="col-md-4">
                <input id="phone_no" name="phone_no" type="text" placeholder="Phone#" class="form-control input-md" required="">
                <span class="text-danger">
                <?php echo isset($_SESSION['error_phone_no']) ? $_SESSION['error_phone_no'] : ''; ?>
                </span> 
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="image">File upload</label>  
            <div class="col-md-4">
                <input id="profile_image" name="profile_image" type="file" 
                class="form-control input-md">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="submit"></label>
            <div class="col-md-4">
                <button id="submit" name="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </fieldset>
</form>
<?php
    include('footer.php');

    if (isset($_SESSION['error'])) {

        $_SESSION = array();
    }
?>